﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirMisiones : MonoBehaviour
{
    public GameObject Panel;

    public void AbrirPanel(){
        if(Panel != null){
            bool isActive = Panel.activeSelf;
            Panel.SetActive(!isActive);
        }
    }
}
