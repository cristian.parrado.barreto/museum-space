﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicPj1 : MonoBehaviour
{
    public float VelocidadMovimiento = 15.0f;
    public float VelocidadRotacion = 200.0f;
    private Animator anim;
    public float x,y;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        x = SimpleInput.GetAxis("Horizontal");
        y = SimpleInput.GetAxis("Vertical");
        
        transform.Rotate(0,x*Time.deltaTime*VelocidadRotacion,0);
        transform.Translate(0,0,y*Time.deltaTime*VelocidadMovimiento);

        anim.SetFloat("VelX",x);
        anim.SetFloat("VelY",y);
    }
}
