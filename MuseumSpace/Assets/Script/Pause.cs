﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{    void Start()
    {
    }

    // Update is called once per frame
   public void pause(){
       Time.timeScale = (true) ? 0 : 1f;
   }
   public void ToContinue(){
       Time.timeScale = (false) ? 0 : 1f;
   }
}
