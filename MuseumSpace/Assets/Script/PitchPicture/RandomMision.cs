﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomMision : MonoBehaviour
{
    public Text txt1;
    public Text txt2;
    public Text txt3;
    public Image imgMission1;
    public Image imgMission2;
    public Image imgMission3;
    public Text Category1;
    public Text Category2;
    public Text Category3;
    public Text title1;
    public Text title2;
    public Text title3;
        void Start()
    {
        int [] elemento = new int[3];
        string[] tittles = new string[] {"The Very Large Telescope snaps a stellar nursery and celebrates fifteen years of operations"
        , "Stellar Nursery Blooms into View", "Thor’s Helmet Nebula imaged on the occasion of ESO’s 50th anniversary", "A 340-million pixel starscape from Paranal"
        , "The Helix Nebula", "The Horsehead Nebula*", "A deep infrared view of the Orion Nebula from HAWK-I", "The Carina Nebula imaged by the VLT Survey Telescope"
        , "ESO’s VLT reveals the Carina Nebula's hidden secrets", "Messier 78: a reflection nebula in Orion", "VLT looks into the eyes of the Virgin"
        , "The centre of the Milky Way*", "A deep look at the strange galaxy Centaurus A", "The Milky Way panorama", "Spiral galaxy NGC 1232"
        , "VISTA gigapixel mosaic of the central parts of the Milky Way", "Antennae Galaxies composite of ALMA and Hubble observations", "A portrait of a beauty"
        , "Wide Field Imager view of the southern spiral NGC 300", "Hidden from view", "Total solar eclipse, La Silla Observatory, 2019", "New image of comet ISON"
        , "The waning moon", "Lunar Eclipse @ ESO", "Jupiter imaged using the VISIR instrument on the VLT", "Sharpening up Jupiter", "Comet Hale-Bopp"
        , "Neptune from the VLT with MUSE/GALACSI Narrow Field Mode adaptive optics", "Comet West, March 1976", "An image of the strange asteroid Lutetia from the ESA Rosetta probe"};
        int NumeroLimite=30;
        elemento [0] = Random.Range(0,NumeroLimite);
		for(int cont1 = 1;cont1<elemento.Length;cont1++)
        {
            int numeroAzar = Random.Range(0,NumeroLimite);
			int cont2 = 0;
            while(cont2<cont1)
			{
                if (elemento[cont2]==numeroAzar)
				{
                    numeroAzar= Random.Range(0,NumeroLimite);
                    cont2 = 0;
					continue;
				}
				else
				{
                    elemento [cont1]= numeroAzar;
				}
                cont2++;
			}
            }
            txt1.text=elemento[0]+"";
            imgMission1.sprite = Resources.Load<Sprite>("Sprites/"+elemento[0]);
            title1.text=tittles[elemento[0]];
            txt2.text=elemento[1]+"";
            imgMission2.sprite = Resources.Load<Sprite>("Sprites/"+elemento[1]);
            title2.text=tittles[elemento[1]];
            txt3.text=elemento[2]+"";
            imgMission3.sprite = Resources.Load<Sprite>("Sprites/"+elemento[2]); 
            title3.text=tittles[elemento[2]];
            string[] category = new string[3];
            for(int i = 0;i<elemento.Length;i++){
                if(elemento[i]<=9){
                    category[i]="Nebulae";
                } if (elemento[i]>=10 && elemento[i]<=19){
                    category[i]="Galaxies";
                } if (elemento[i]>=20){
                    category[i]="Solar System";
                }
            }
            Category1.text="Category: " + category[0];
            Category2.text="Category: " + category[1];
            Category3.text="Category: " + category[2];

    }

    void Update()
    {
        
    }
}
