﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerLupa : MonoBehaviour
{

    public GameObject BtnLupa;
    public GameObject PnlDescripcion;
    public Text txtTitulo;
    public Text txtDescripcion;
    public Image imgCuadro;
    public int idPicture;
    public Text credits;
  
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerPrefs.SetString("idSuper",idPicture+"");
            if (idPicture == 0)
            {
                txtTitulo.text = "The Very Large Telescope snaps a stellar nursery and celebrates fifteen years of operations";
                txtDescripcion.text = "This intriguing new view of a spectacular stellar nursery IC 2944 is being released to celebrate a milestone: 15 years of ESO’s Very Large Telescope. This image also shows a group of thick clouds of dust known as the Thackeray globules silhouetted against the pale pink glowing gas of the nebula.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/0");
                credits.text = "Credits: ESO";
                PlayerPrefs.SetString("id0",txtTitulo.text);
            }
            else if (idPicture == 1)
            {
                txtTitulo.text = "Stellar Nursery Blooms into View";
                txtDescripcion.text = "The OmegaCAM imager on ESO’s VLT Survey Telescope has captured this glittering view of the stellar nursery called Sharpless 29. Many astronomical phenomena can be seen in this giant image, including cosmic dust and gas clouds that reflect, absorb, and re-emit the light of hot young stars within the nebula.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/1");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 2)
            {
                txtTitulo.text = "Thor’s Helmet Nebula imaged on the occasion of ESO’s 50th anniversary";
                txtDescripcion.text = "This VLT image of the Thor’s Helmet Nebula was taken on the occasion of ESO’s 50th Anniversary, 5 October 2012, with the help of Brigitte Bailleul — winner of the Tweet Your Way to the VLT! competition. The observations were broadcast live over the internet from the Paranal Observatory in Chile. This object, also known as NGC 2359, lies in the constellation of Canis Major (The Great Dog).";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/2");
                credits.text = "Credits: ESO/B. Bailleul";
            }
            else if (idPicture == 3)
            {
                txtTitulo.text = "A 340-million pixel starscape from Paranal";
                txtDescripcion.text = "The second of three images of ESO’s GigaGalaxy Zoom project is a new and wonderful 340-million-pixel vista of the central parts of our galactic home, a 34 by 20-degree wide image that provides us with a view as experienced by amateur astronomers around the world. Taken by Stéphane Guisard, an ESO engineer and world-renowned astrophotographer, from Cerro Paranal, home of ESO’s Very Large Telescope.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/3");
                credits.text = "Credits: ESO/S. Guisard";
            }
            else if (idPicture == 4)
            {
                txtTitulo.text = "The Helix Nebula*";
                txtDescripcion.text = "This colour-composite image of the Helix Nebula (NGC 7293) was created from images obtained using the Wide Field Imager (WFI), an astronomical camera attached to the 2.2-metre Max-Planck Society/ESO telescope at the La Silla observatory in Chile. The blue-green glow in the centre of the Helix comes from oxygen atoms shining under effects of the intense ultraviolet radiation of the 120 000 degree Celsius central star and the hot gas.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/4");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 5)
            {
                txtTitulo.text = "The Horsehead Nebula*";
                txtDescripcion.text = "A reproduction of a composite colour image of the Horsehead Nebula and its immediate surroundings. It is based on three exposures in the visual part of the spectrum with the FORS2 multi-mode instrument at the 8.2-m KUEYEN telescope at Paranal.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/5");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 6)
            {
                txtTitulo.text = "A deep infrared view of the Orion Nebula from HAWK-I";
                txtDescripcion.text = "This spectacular image of the Orion Nebula star-formation region was obtained from multiple exposures using the HAWK-I infrared camera on ESO’s Very Large Telescope in Chile. This is the deepest view ever of this region and reveals more very faint planetary-mass objects than expected.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/6");
                credits.text = "Credits: ESO/H. Drass et al.";
            }
            else if (idPicture == 7)
            {
                txtTitulo.text = "The Carina Nebula imaged by the VLT Survey Telescope";
                txtDescripcion.text = "The spectacular star-forming Carina Nebula has been captured in great detail by the VLT Survey Telescope at ESO’s Paranal Observatory. This picture was taken with the help of Sebastián Piñera, President of Chile, during his visit to the observatory on 5 June 2012 and released on the occasion of the new telescope’s inauguration in Naples on 6 December 2012.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/7");
                credits.text = "Credits: ESO. Acknowledgement: VPHAS+ Consortium/Cambridge Astronomical Survey Unit";
            }
            else if (idPicture == 8)
            {
                txtTitulo.text = "ESO’s VLT reveals the Carina Nebula's hidden secrets";
                txtDescripcion.text = "This broad image of the Carina Nebula, a region of massive star formation in the southern skies, was taken in infrared light using the HAWK-I camera on ESO’s Very Large Telescope. Many previously hidden features, scattered across a spectacular celestial landscape of gas, dust and young stars, have emerged.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/8");
                credits.text = "Credits: ESO/T. Preibisch";
            }
            else if (idPicture == 9)
            {
                txtTitulo.text = "Messier 78: a reflection nebula in Orion";
                txtDescripcion.text = "This new image of the reflection nebula Messier 78 was captured using the Wide Field Imager camera on the MPG/ESO 2.2-metre telescope at the La Silla Observatory, Chile. This colour picture was created from many monochrome exposures taken through blue, yellow/green and red filters, supplemented by exposures through a filter that isolates light from glowing hydrogen gas.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/9");
                credits.text = "Credits: ESO/Igor Chekalin";
            }
            else if (idPicture == 10)
            {
                txtTitulo.text = "VLT looks into the eyes of the Virgin";
                txtDescripcion.text = "This striking image, taken with the FORS2 instrument on the Very Large Telescope, shows a beautiful yet peculiar pair of galaxies, NGC 4438 and NGC 4435, nicknamed The Eyes. The larger of these, at the top of the picture, NGC 4438, is thought to have once been a spiral galaxy that was strongly deformed by collisions in the relatively recent past. The two galaxies belong to the Virgo Cluster and are about 50 million light-years away.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/10");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 11)
            {
                txtTitulo.text = "The centre of the Milky Way*";
                txtDescripcion.text = "The central parts of our Galaxy, the Milky Way, as observed in the near-infrared with the NACO instrument on ESO's Very Large Telescope. By following the motions of the most central stars over more than 16 years, astronomers were able to determine the mass of the supermassive black hole that lurks there.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/11");
                credits.text = "Credits: ESO/S. Gillessen et al.";
            }
            else if (idPicture == 12)
            {
                txtTitulo.text = "A deep look at the strange galaxy Centaurus A";
                txtDescripcion.text = "The peculiar galaxy Centaurus A (NGC 5128) is pictured in this image taken with the Wide Field Imager attached to the MPG/ESO 2.2-metre telescope at the La Silla Observatory in Chile. With a total exposure time of more than 50 hours this is probably the deepest view of this peculiar and spectacular object ever created.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/12");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 13)
            {
                txtTitulo.text = "The Milky Way panorama";
                txtDescripcion.text = "This magnificent 360-degree panoramic image, covering the entire southern and northern celestial sphere, reveals the cosmic landscape that surrounds our tiny blue planet. This gorgeous starscape serves as the first of three extremely high-resolution images featured in the GigaGalaxy Zoom project, launched by ESO.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/13");
                credits.text = "Credits: ESO/S. Brunier";
            }
            else if (idPicture == 14)
            {
                txtTitulo.text = "Spiral galaxy NGC 1232";
                txtDescripcion.text = "This spectacular image of the large spiral galaxy NGC 1232 was obtained on September 21, 1998, during a period of good observing conditions. It is based on three exposures in ultra-violet, blue and red light, respectively. The colours of the different regions are well visible : the central areas contain older stars of reddish colour, while the spiral arms are populated by young, blue stars and many star-forming regions.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/14");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 15)
            {
                txtTitulo.text = "VISTA gigapixel mosaic of the central parts of the Milky Way";
                txtDescripcion.text = "This striking view of the central parts of the Milky Way was obtained with the VISTA survey telescope at ESO’s Paranal Observatory in Chile. This huge picture is 108 200 by 81 500 pixels and contains nearly nine billion pixels. It was created by combining thousands of individual images from VISTA, taken through three different infrared filters, into a single monumental mosaic.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/15");
                credits.text = "Credits: ESO/VVV Survey/D. Minniti Acknowledgement: Ignacio Toledo, Martin Kornmesser";
            }
            else if (idPicture == 16)
            {
                txtTitulo.text = "Antennae Galaxies composite of ALMA and Hubble observations";
                txtDescripcion.text = "The Antennae Galaxies (also known as NGC 4038 and 4039) are a pair of distorted colliding spiral galaxies about 70 million light-years away, in the constellation of Corvus (The Crow). This view combines ALMA observations, made in two different wavelength ranges during the observatory’s early testing phase, with visible-light observations from the NASA/ESA Hubble Space Telescope.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/16");
                credits.text = "Credits: ALMA (ESO/NAOJ/NRAO). Visible light image: the NASA/ESA Hubble Space Telescope";
            }
            else if (idPicture == 17)
            {
                txtTitulo.text = "A portrait of a beauty";
                txtDescripcion.text = "Nuzzled in the chest of the constellation Virgo (the Virgin) lies a beautiful cosmic gem — the galaxy Messier 61. This glittering spiral galaxy is aligned face-on towards Earth, thus presenting us with a breathtaking view of its structure. The gas and dust of the intricate spiral arms are studded with billions of stars.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/17");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 18)
            {
                txtTitulo.text = "Wide Field Imager view of the southern spiral NGC 300";
                txtDescripcion.text = "This picture of the spectacular southern spiral galaxy NGC 300 was taken using the Wide Field Imager (WFI) at ESO’s La Silla Observatory in Chile. It was assembled from many individual images through a large set of different filters over many observing nights, spanning several years.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/18");
                credits.text = "Credits: ESO";
            }
            else if (idPicture == 19)
            {
                txtTitulo.text = "Hidden from view";
                txtDescripcion.text = "This ESO Picture of the Week shows the centre of a galaxy named NGC 5643. This galaxy is located 55 million light-years from Earth in the constellation of Lupus (The Wolf), and is known as a Seyfert galaxy. Seyfert galaxies have very luminous centres — thought to be powered by material being accreted onto a supermassive black hole lurking within — that can also be shrouded and obscured by clouds of dust and intergalactic material.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/19");
                credits.text = "Credits: ESO/A. Alonso-Herrero et al.; ALMA (ESO/NAOJ/NRAO)";
            }
            else if (idPicture == 20)
            {
                txtTitulo.text = "Total solar eclipse, La Silla Observatory, 2019";
                txtDescripcion.text = "On 2 July 2019 a total solar eclipse passed over ESO’s La Silla Observatory in Chile. The eclipse lasted roughly two and a half hours, with almost two minutes of totality at 20:39 UT, and was visible across a narrow band of Chile and Argentina. To celebrate this rare event ESO invited 1000 people, including dignitaries, school children, the media, researchers, and the general public, to come to the Observatory to watch the eclipse from this unique location.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/20");
                credits.text = "Credits: ESO/P. Horálek";
            }
            else if (idPicture == 21)
            {
                txtTitulo.text = "New image of comet ISON";
                txtDescripcion.text = "This new view of Comet C/2012 S1 (ISON) was taken with the TRAPPIST–South national telescope at ESO's La Silla Observatory on the morning of Friday 15 November 2013. Comet ISON was first spotted in our skies in September 2012, and will make its closest approach to the Sun in late November 2013.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/21");
                credits.text = "Credits: TRAPPIST/E. Jehin/ESO";
            }
            else if (idPicture == 22)
            {
                txtTitulo.text = "The waning moon";
                txtDescripcion.text = "A series of short exposures through a near-infrared filtre was obtained of the waning Moon at sunrise on January 12 (at about 10 hrs UT), i.e. about 5 days before New Moon (24.3 days 'old'). As can be seen in the image, the edge of the full field-of-view is about the size of the diameter of the Moon.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/22");
                credits.text = "Credits: ESO/D. Baade";
            }
            else if (idPicture == 23)
            {
                txtTitulo.text = "Lunar Eclipse @ ESO";
                txtDescripcion.text = "During the early evening of 7 August, a partial lunar eclipse was visible in the sky above the ESO Headquarters in Garching bei München, Germany. A lunar eclipse occurs when the Earth, Moon and Sun are aligned, and the Earth casts a shadow on the Moon. This time, only a small part of the Moon entered the Earth’s inner shadow, the umbra, but it still made for a stunning view.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/23");
                credits.text = "Credits: ESO/P. Horálek";
            }
            else if (idPicture == 24)
            {
                txtTitulo.text = "Jupiter imaged using the VISIR instrument on the VLT";
                txtDescripcion.text = "In preparation for the imminent arrival of NASA’s Juno spacecraft in July 2016, astronomers used ESO’s Very Large Telescope to obtain spectacular new infrared images of Jupiter using the VISIR instrument. They are part of a campaign to create high-resolution maps of the giant planet to inform the work to be undertaken by Juno over the following months, helping astronomers to better understand the gas giant.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/24");
                credits.text = "Credits: ESO/L. Fletcher";
            }
            else if (idPicture == 25)
            {
                txtTitulo.text = "Sharpening up Jupiter";
                txtDescripcion.text = "Amazing image of Jupiter taken in infrared light on the night of 17 August 2008 with the Multi-Conjugate Adaptive Optics Demonstrator (MAD) prototype instrument mounted on ESO's Very Large Telescope. This false colour photo is the combination of a series of images taken over a time span of about 20 minutes, through three different filters (2, 2.14, and 2.16 microns).";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/25");
                credits.text = "Credits: ESO/F. Marchis, M. Wong, E. Marchetti, P. Amico, S. Tordo";
            }
            else if (idPicture == 26)
            {
                txtTitulo.text = "Comet Hale-Bopp";
                txtDescripcion.text = "Comet C/1995 O1 Hale-Bopp, on March 14, 1997. On this photo obtained with a telelens, the dust tail fans out to the right, while the bright, well separated blue ion tail is pointing straight away from the Sun.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/26");
                credits.text = "Credits: ESO/E. Slawik";
            }
            else if (idPicture == 27)
            {
                txtTitulo.text = "Neptune from the VLT with MUSE/GALACSI Narrow Field Mode adaptive optics";
                txtDescripcion.text = "This image of the planet Neptune was obtained during the testing of the Narrow-Field adaptive optics mode of the MUSE/GALACSI instrument on ESO’s Very Large Telescope. The corrected image is sharper than a comparable image from the NASA/ESA Hubble Space Telescope.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/27");
                credits.text = "Credits: ESO/P. Weilbacher (AIP)";
            }
            else if (idPicture == 28)
            {
                txtTitulo.text = "Comet West, March 1976";
                txtDescripcion.text = "Comet West was discovered in photographs by Richard West on August 10, 1975. It reached peak brightness in March 1976. During its peak brightness, observers reported that it was bright enough to study during full daylight. Despite its spectacular appearance, it did't cause much expectation among the popular media. The comet has an estimated orbital period of 558,000 years.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/28");
                credits.text = "Credits: P. Stättmayer/ESO";
            }
            else if (idPicture == 29)
            {
                txtTitulo.text = "An image of the strange asteroid Lutetia from the ESA Rosetta probe";
                txtDescripcion.text = "This image of the unusual asteroid Lutetia was taken by ESA’s Rosetta probe during its closest approach in July 2010. Lutetia, which is about 100 kilometres across, seems to be a leftover fragment of the same original material that formed the Earth, Venus and Mercury. It is now part of the main asteroid belt, between the orbits of Mars and Jupiter, but its composition suggests that it was originally much closer to the Sun.";
                imgCuadro.sprite = Resources.Load<Sprite>("Sprites/29");
                credits.text = "Credits: ESA 2010 MPS for OSIRIS Team MPS/UPD/LAM/IAA/RSSD/INTA/UPM/DASP/IDA";
            }
            if (BtnLupa != null)
            {
                BtnLupa.SetActive(!BtnLupa.activeSelf);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (BtnLupa != null)
            {
                BtnLupa.SetActive(!BtnLupa.activeSelf);
            }
            PnlDescripcion.SetActive(BtnLupa.activeSelf);
        }
    }
}
