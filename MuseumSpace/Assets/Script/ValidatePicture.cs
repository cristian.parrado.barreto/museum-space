﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ValidatePicture : MonoBehaviour
{
    public Text findPicture1;
    public Toggle check1;
    public Text findPicture2;
    public Toggle check2;
    public Text findPicture3;
    public Toggle check3;
    public GameObject Gamer;

    public void validarID(){
        string idPicture = PlayerPrefs.GetString("idSuper");
        int idP=int.Parse(idPicture);
        int find1=int.Parse(findPicture1.text);
        int find2=int.Parse(findPicture2.text);
        int find3=int.Parse(findPicture3.text);
        if(idP==find1){
            check1.isOn=true;
        }
        else if(idP==find2){
            check2.isOn=true;
        }
        else if(idP==find3){
            check3.isOn=true;
        }
        if(check1.isOn==true && check2.isOn==true && check3.isOn==true){
            Gamer.SetActive(!Gamer.activeSelf);
            Debug.Log("Has Ganado!");
        }
    }
}
