# MUSEUM SPACE


### Abstract

_It is a mobile application as a virtual learning object to publicize the multiple images found on the platform of European Southern Observatory (www.eso.org), consists of looking for images in a museum in the shortest possible time, additionally the description will be shown of each image._
###### Keyboard: _Image, Espace_
### Installation 🔧

_Initially you must enter the application store, for android it is the play store and in the case of ios it is the app store, later the application is searched and the download button is given, the application will request permissions, the permissions are accepted and the application will be installed automatically_

```
1.) Enter the store
    2.) Search application
        3.) Download application
```
### Project integrants

###### Cristian Andres Parrado Barreto
###### Candido Stiven Moreno Cerrato
## Built with
_Below is a list of the tools that were implemented for the creation of the app_

* [Unity 3D](https://unity.com/es) - Video game platform
* [Visual studio CODE](https://code.visualstudio.com/?wt.mc_id=DX_841432) - Development environment
* [GitLab](https://gitlab.com/) - version control web service

## License 📄

This project is under License GPL 3.0 - look at the file [License.txt](https://gitlab.com/cristian.parrado.barreto/museum-space/blob/master/LICENSE) for more information
